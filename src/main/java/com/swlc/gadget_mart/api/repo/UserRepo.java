package com.swlc.gadget_mart.api.repo;

import com.swlc.gadget_mart.api.dto.UserDTO;

import java.util.List;

public interface UserRepo {

    UserDTO authenticateUser(String username) throws Exception;

    boolean createUser(UserDTO user) throws Exception;

    UserDTO findUser(String userName, String userType) throws Exception;

    boolean updateUser(UserDTO userDTO) throws Exception;

    List<UserDTO> getAllUsers() throws Exception;
}
