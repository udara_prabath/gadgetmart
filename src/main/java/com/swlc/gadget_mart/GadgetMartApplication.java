package com.swlc.gadget_mart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GadgetMartApplication {

    public static void main(String[] args) {
        SpringApplication.run(GadgetMartApplication.class, args);
    }

}
