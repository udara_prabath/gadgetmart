package com.swlc.gadget_mart.api.repo;

import com.swlc.gadget_mart.api.dto.CategoryDTO;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface CategoryRepo {
    List<CategoryDTO> getAllCategories() throws Exception;
}
