package com.swlc.gadget_mart.web_service.repo;

import com.swlc.gadget_mart.api.dto.ProviderDTO;
import com.swlc.gadget_mart.api.repo.ProviderRepo;
import com.swlc.gadget_mart.web_service.dto.Item;
import com.swlc.gadget_mart.web_service.dto.Items;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Component
public class ItemRepository {

    @Autowired
    private ProviderRepo providerRepo;

    @PostConstruct
    public void initData() {

    }

    public ArrayList<Item> findItems() {
        ArrayList<Item> items = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();

        try {
            List<ProviderDTO> allProviders = providerRepo.getAllProviders();
            for (ProviderDTO providerDTO : allProviders) {
                Items providerItems = restTemplate.getForObject(providerDTO.getUrl(), Items.class);
                if (providerItems != null && providerItems.getItems() != null)
                    items.addAll(providerItems.getItems());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Items abans = restTemplate.getForObject(
//                "http://128.199.30.5:8080/abans/items",
//                Items.class
//        );
//
//        Items singer = restTemplate.getForObject(
//                "http://128.199.30.5:8080/singer/items",
//                Items.class
//        );
//        Items softLogic = restTemplate.getForObject(
//                "http://128.199.30.5:8080/softlogic/items",
//                Items.class
//        );

        long seed = System.nanoTime();
        Collections.shuffle(items, new Random(seed));

        System.out.println(items.size());
        System.out.println(items);

        return items;
    }
}
