package com.swlc.gadget_mart.api.repo;

import com.swlc.gadget_mart.api.dto.OrderDTO;

import java.util.List;

public interface OrderRepo {
    boolean placeOrder(OrderDTO orderDTO) throws Exception;

    boolean updateOrder(OrderDTO orderDTO) throws Exception;

    List<OrderDTO> getAllOrder(String userId) throws Exception;

    List<OrderDTO> getAllOrder() throws Exception;
}
