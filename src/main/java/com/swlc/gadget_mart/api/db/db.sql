drop DATABASE gadget_mart;

CREATE DATABASE gadget_mart;

USE gadget_mart;

CREATE TABLE service_provider(
provider_id int(10) NOT NULL AUTO_INCREMENT,
name varchar(225) NOT NULL,
url varchar(225) NOT NULL,
CONSTRAINT PRIMARY KEY(provider_id)
) ENGINE=INNODB;

CREATE TABLE category(
category_id int(10) NOT NULL AUTO_INCREMENT,
name varchar(225) NOT NULL,
CONSTRAINT PRIMARY KEY(category_id)
) ENGINE=INNODB;

CREATE TABLE brand(
brand_name varchar(225) NOT NULL,
CONSTRAINT PRIMARY KEY(brand_name)
) ENGINE=INNODB;

CREATE TABLE category_brand(
category_brand_id int NOT NULL AUTO_INCREMENT,
category_id int(100) NOT NULL,
brand_name varchar(225) NOT NULL,
CONSTRAINT PRIMARY KEY(category_brand_id),
CONSTRAINT FOREIGN KEY(category_id) REFERENCES category(category_id) on update cascade on delete cascade,
CONSTRAINT FOREIGN KEY(brand_name) REFERENCES brand(brand_name) on update cascade on delete cascade
) ENGINE=INNODB;

CREATE TABLE user_login (
    user_id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(500) NOT NULL,
    userType VARCHAR(10) NOT NULL,
    userName VARCHAR(150) NOT NULL,
    password VARCHAR(100) NOT NULL,
    address VARCHAR(150) NOT NULL,
    contact VARCHAR(11) NOT NULL,
    email VARCHAR(150) NOT NULL,
    PRIMARY KEY (user_id),
    UNIQUE(userName)
)ENGINE=InnoDB;

insert into user_login values (0,'Super Admin','ADMIN','admin','12345','panadura','0756470925','admin@admin.com');

CREATE TABLE orders(
    order_id int(10) NOT NULL AUTO_INCREMENT,
    user_id int(10) NOT NULL,
    address VARCHAR(150) NOT NULL,
    contact VARCHAR(11) NOT NULL,
    paymentMethod VARCHAR(11) NOT NULL,
    totalCost double NOT NULL,
    status VARCHAR(11) NOT NULL DEFAULT 'PENDING',
    CONSTRAINT PRIMARY KEY(order_id),
    CONSTRAINT FOREIGN KEY(user_id) REFERENCES user_login(user_id) on update cascade on delete cascade
) ENGINE=INNODB;

CREATE TABLE order_detail(
    order_detail_id int(10) NOT NULL AUTO_INCREMENT,
    order_id int(10) NOT NULL,
    name VARCHAR (225) NOT NULL,
    description VARCHAR (225) NOT NULL,
    image VARCHAR (225) NOT NULL,
    price DOUBLE NOT NULL,
    deliveryCost DOUBLE NOT NULL,
    brand VARCHAR (225) NOT NULL,
    category VARCHAR (225) NOT NULL,
    discount int(10) NOT NULL,
    shop VARCHAR (225) NOT NULL,
    soldOut BOOL NOT NULL,
    warranty VARCHAR (225) NOT NULL,
    qty int (10) NOT NULL,
    CONSTRAINT PRIMARY KEY(order_detail_id),
    CONSTRAINT FOREIGN KEY(order_id) REFERENCES orders(order_id) on update cascade on delete cascade
) ENGINE=INNODB;

insert into category (category_id, name) values (1,'Computers & Mobile');
insert into category (category_id, name) values (2,'TV & Audio');
insert into category (category_id, name) values (3,'Kitchen Appliances');
insert into category (category_id, name) values (4,'Home Appliances');
insert into category (category_id, name) values (5,'Fitness');
insert into category (category_id, name) values (6,'Refrigerator, Air Conditioner, Washing Machines');

insert into brand (brand_name) VALUES ('Huawei');
insert into brand (brand_name) VALUES ('Apple');
insert into brand (brand_name) VALUES ('Samsung');
insert into brand (brand_name) VALUES ('Dell');
insert into brand (brand_name) VALUES ('Asus');
insert into brand (brand_name) VALUES ('Canon');
insert into brand (brand_name) VALUES ('Energizer');
insert into brand (brand_name) VALUES ('UNIC');
insert into brand (brand_name) VALUES ('Acer');
insert into brand (brand_name) VALUES ('Singer');
insert into brand (brand_name) VALUES ('Sony');
insert into brand (brand_name) VALUES ('HP');
insert into brand (brand_name) VALUES ('Skyworth');
insert into brand (brand_name) VALUES ('TCL');
insert into brand (brand_name) VALUES ('Celebrat');
insert into brand (brand_name) VALUES ('Nikai');
insert into brand (brand_name) VALUES ('Yamaha');
insert into brand (brand_name) VALUES ('Sharp');
insert into brand (brand_name) VALUES ('Hitachi');
insert into brand (brand_name) VALUES ('Yison');
insert into brand (brand_name) VALUES ('Sunpin');
insert into brand (brand_name) VALUES ('Marshall');
insert into brand (brand_name) VALUES ('Tefal');
insert into brand (brand_name) VALUES ('Prestige');
insert into brand (brand_name) VALUES ('Moulinex');
insert into brand (brand_name) VALUES ('Beko');
insert into brand (brand_name) VALUES ('Hafele');
insert into brand (brand_name) VALUES ('Sisil');
insert into brand (brand_name) VALUES ('Kenwood');
insert into brand (brand_name) VALUES ('Indesit');
insert into brand (brand_name) VALUES ('Regnis');
insert into brand (brand_name) VALUES ('Panasonic');
insert into brand (brand_name) VALUES ('Thermos');
insert into brand (brand_name) VALUES ('Welling');
insert into brand (brand_name) VALUES ('Insinkerator');
insert into brand (brand_name) VALUES ('Whirlpool');
insert into brand (brand_name) VALUES ('Karcher');
insert into brand (brand_name) VALUES ('Quantum');
insert into brand (brand_name) VALUES ('Mitsubishi');

insert into category_brand (category_id, brand_name) VALUES (1,'Huawei');
insert into category_brand (category_id, brand_name) VALUES (1,'Apple');
insert into category_brand (category_id, brand_name) VALUES (1,'Samsung');
insert into category_brand (category_id, brand_name) VALUES (1,'Dell');
insert into category_brand (category_id, brand_name) VALUES (1,'Asus');
insert into category_brand (category_id, brand_name) VALUES (1,'Canon');
insert into category_brand (category_id, brand_name) VALUES (1,'Energizer');
insert into category_brand (category_id, brand_name) VALUES (1,'UNIC');
insert into category_brand (category_id, brand_name) VALUES (1,'Acer');
insert into category_brand (category_id, brand_name) VALUES (1,'Singer');
insert into category_brand (category_id, brand_name) VALUES (1,'Sony');
insert into category_brand (category_id, brand_name) VALUES (1,'HP');

-- tv
insert into category_brand (category_id, brand_name) VALUES (2, 'Sony');
insert into category_brand (category_id, brand_name) VALUES (2, 'Singer');
insert into category_brand (category_id, brand_name) VALUES (2, 'UNIC');
insert into category_brand (category_id, brand_name) VALUES (2, 'Samsung');
insert into category_brand (category_id, brand_name) VALUES (2, 'Skyworth');
insert into category_brand (category_id, brand_name) VALUES (2, 'TCL');
insert into category_brand (category_id, brand_name) VALUES (2, 'Celebrat');
insert into category_brand (category_id, brand_name) VALUES (2, 'Huawei');
insert into category_brand (category_id, brand_name) VALUES (2, 'Nikai');
insert into category_brand (category_id, brand_name) VALUES (2, 'Yamaha');
insert into category_brand (category_id, brand_name) VALUES (2, 'Sharp');
insert into category_brand (category_id, brand_name) VALUES (2, 'Hitachi');
insert into category_brand (category_id, brand_name) VALUES (2, 'Yison');
insert into category_brand (category_id, brand_name) VALUES (2, 'Sunpin');
insert into category_brand (category_id, brand_name) VALUES (2, 'Marshall');

-- kit
insert into category_brand (category_id, brand_name) VALUES (3,'Singer');
insert into category_brand (category_id, brand_name) VALUES (3,'Tefal');
insert into category_brand (category_id, brand_name) VALUES (3,'Prestige');
insert into category_brand (category_id, brand_name) VALUES (3,'Moulinex');
insert into category_brand (category_id, brand_name) VALUES (3,'Beko');
insert into category_brand (category_id, brand_name) VALUES (3,'Hafele');
insert into category_brand (category_id, brand_name) VALUES (3,'Sisil');
insert into category_brand (category_id, brand_name) VALUES (3,'Kenwood');
insert into category_brand (category_id, brand_name) VALUES (3,'Indesit');
insert into category_brand (category_id, brand_name) VALUES (3,'Regnis');
insert into category_brand (category_id, brand_name) VALUES (3,'Panasonic');
insert into category_brand (category_id, brand_name) VALUES (3,'Nikai');
insert into category_brand (category_id, brand_name) VALUES (3,'Thermos');
insert into category_brand (category_id, brand_name) VALUES (3,'Samsung');
insert into category_brand (category_id, brand_name) VALUES (3,'Welling');
insert into category_brand (category_id, brand_name) VALUES (3,'Sharp');
insert into category_brand (category_id, brand_name) VALUES (3,'Insinkerator');
insert into category_brand (category_id, brand_name) VALUES (3,'Hitachi');
insert into category_brand (category_id, brand_name) VALUES (3,'Whirlpool');


-- home
insert into category_brand (category_id, brand_name) VALUES  (4,'Tefal');
insert into category_brand (category_id, brand_name) VALUES  (4,'Singer');
insert into category_brand (category_id, brand_name) VALUES  (4,'Karcher');
insert into category_brand (category_id, brand_name) VALUES  (4,'Hitachi');
insert into category_brand (category_id, brand_name) VALUES  (4,'Kenwood');
insert into category_brand (category_id, brand_name) VALUES  (4,'Nikai');
insert into category_brand (category_id, brand_name) VALUES  (4,'UNIC');
insert into category_brand (category_id, brand_name) VALUES  (4,'Sisil');
insert into category_brand (category_id, brand_name) VALUES  (4,'Beko');

-- fitness
insert into category_brand (category_id, brand_name) VALUES  (5,'Quantum');
insert into category_brand (category_id, brand_name) VALUES  (5,'Singer');

-- wasine
insert into category_brand (category_id, brand_name) VALUES  (6,'Singer');
insert into category_brand (category_id, brand_name) VALUES  (6,'Sisil');
insert into category_brand (category_id, brand_name) VALUES  (6,'Samsung');
insert into category_brand (category_id, brand_name) VALUES  (6,'Hitachi');
insert into category_brand (category_id, brand_name) VALUES  (6,'Beko');
insert into category_brand (category_id, brand_name) VALUES  (6,'Mitsubishi');
insert into category_brand (category_id, brand_name) VALUES  (6,'Whirlpool');
insert into category_brand (category_id, brand_name) VALUES  (6,'Indesit');
insert into category_brand (category_id, brand_name) VALUES  (6,'Nikai');
