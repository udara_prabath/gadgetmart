package com.swlc.gadget_mart.api.db;

class DatabaseConstants {

    static final String DB_URL = "jdbc:mysql://localhost:3306/gadget_mart?characterEncoding=latin1";

    static final String DRIVER = "com.mysql.jdbc.Driver";

    static final String USERNAME = "root";

    static final String PASSWORD = "royalcops"; // Local database password

}
