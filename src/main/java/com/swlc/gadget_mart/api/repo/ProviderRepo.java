package com.swlc.gadget_mart.api.repo;

import com.swlc.gadget_mart.api.dto.ProviderDTO;

import java.util.List;

public interface ProviderRepo {
    boolean createProvider(ProviderDTO providerDTO) throws Exception;

    List<ProviderDTO> getAllProviders() throws Exception;

    boolean updateProvider(ProviderDTO providerDTO) throws Exception;

    boolean deleteProvider(ProviderDTO providerDTO) throws Exception;
}
